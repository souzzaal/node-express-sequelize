const app = require('../../app')
const factory = require('../../database/factories/')
const request = require('supertest')
const { Task } = require('../../app/models')


beforeAll(() => {
	user = (async () => await factory.create('User'))()
});

const dataProvider = [
	// desc, dataset
	['sem dados', {}],
	['sem title', { body: 'b', user_id: 1 }],
	['com title nulo', { title: null, body: 'b', user_id: 1 }],
	['com title vazio', { title: '', body: 'b', user_id: 1 }],
	['sem body', { title: '', user_id: 1 }],
	['com body nulo', { title: 't', body: null, user_id: 1 }],
	['com body vazio', { title: 't', body: '', user_id: 1 }],
	['sem usuario', { title: 't', body: 'b' }],
	['com usuario inexistente', { title: 't', body: 'b', user_id: 0 }],
	['com usuario nao numerico', { title: 't', body: 'b', user_id: 'a' }],
	['com tags nao sendo um array', { title: 't', body: 'b', user_id: 1, tags: null }]
]

jest.setTimeout(15000)

describe('Task', () => {

	it('deve retornar um array de tasks quando existem tasks cadastradas', async () => {

		const tasks = await factory.createMany('Task', 5)
		const tags = await factory.createMany('Tag', 2)

		await tasks[0].setTags(tags)

		const response = await request(app)
			.get('/tasks')
			.send()

		expect(response.status).toBe(200)
		const data = response.body.data

		expect(data.length).toBeGreaterThanOrEqual(1)

		data.forEach(task => {
			expect(task).toEqual(expect.objectContaining({
				id: expect.any(Number),
				title: expect.any(String),
				body: expect.any(String),
				tags: expect.any(Array),
				user: expect.objectContaining({
					id: expect.any(Number),
					name: expect.any(String),
					email: expect.any(String),
				})
			}))

			if (task.tags.length > 0) {
				task.tags.forEach(tag => {
					expect(tag).toEqual(expect.objectContaining({
						id: expect.any(Number),
						name: expect.any(String),
					}))
				})

			}
		});
	})

	it('deve retornar um array de tasks de um determinado ususario', async () => {

		const user = await factory.create('User')
		await factory.createMany('Task', 5)
		await factory.createMany('Task', 5, {
			user_id: user.id
		})

		const response = await request(app)
			.get(`/tasks?filter[user]=${user.id}`)
			.send()

		expect(response.status).toBe(200)
		const data = response.body.data

		data.forEach(task => {
			expect(user.id).toBe(task.user_id)
		});

	})

	it('deve retornar um array de tasks associadas com uma tag', async () => {

		const tag = await factory.create('Tag', { name: 'minhaTag' })
		const tags = await factory.createMany('Tag', 2)
		tags.push(tag)
		const tasks = await factory.createMany('Task', 5)

		await tasks[0].setTags([tag])
		await tasks[1].setTags(tags)

		const piece = 'Tag'

		const response = await request(app)
			.get(`/tasks?filter[tag]=${piece}`)
			.send()

		expect(response.status).toBe(200)
		const data = response.body.data

		data.forEach(task => {

			/* 	
				Verifica se no array (de tags) retornado existe 
				algum objeto cuja propriedade "name" 
				contenha o (ou parte do) termo pesquisado 
			*/
			expect(task.tags).toEqual(
				expect.arrayContaining([
					expect.objectContaining({
						name: expect.stringContaining(piece)
					})
				])
			)
		});

	})

	it('deve retornar um array de tasks de um determinado usuario associadas com uma tag', async () => {

		const tag = await factory.create('Tag', { name: 'minhaTag' })
		const tags = await factory.createMany('Tag', 2)
		tags.push(tag)

		const tasks = await factory.createMany('Task', 2)
		await tasks[0].setTags([tag])
		await tasks[1].setTags(tags)

		const user = await factory.create('User')
		const userTask1 = await factory.create('Task', { user_id: user.id })
		const userTask2 = await factory.create('Task', { user_id: user.id })
		userTask2.setTags([tag])

		const piece = 'Tag'

		const response = await request(app)
			.get(`/tasks?filter[user]=${user.id}&filter[tag]=${piece}`)
			.send()

		expect(response.status).toBe(200)
		const data = response.body.data

		data.forEach(task => {
			console.log(task.tags);

			expect(task.user_id).toBe(user.id)

			/* 	
				Verifica se no array (de tags) retornado existe 
				algum objeto cuja propriedade "name" 
				contenha o (ou parte do) termo pesquisado 
			*/
			expect(task.tags).toEqual(
				expect.arrayContaining([
					expect.objectContaining({
						name: expect.stringContaining(piece)
					})
				])
			)
		});
	})

	it('deve retornar uma task especifica ao buscar pelo id', async () => {
		const tag = await factory.create('Tag')
		const task = await factory.create('Task')
		await task.setTags([tag.id])

		const response = await request(app)
			.get(`/tasks/${task.id}`)
			.send()

		expect(response.status).toBe(200)

		const data = response.body.data

		expect(data).toEqual(expect.objectContaining({
			id: task.id,
			title: task.title,
			body: task.body,
			tags: expect.arrayContaining([{
				id: tag.id,
				name: tag.name
			}]),
			user: expect.objectContaining({
				id: task.user_id,
				name: expect.any(String),
				email: expect.any(String),
			})
		}))

	})

	it('deve retornar 404 ao buscar task por id nao cadastrado', async () => {
		const response = await request(app)
			.get(`/tasks/0`)
			.send()

		expect(response.status).toBe(404)
	})

	it('deve retornar 201 ao cadastrar apanas task com dados validos', async () => {

		const newTask = await factory.build('Task')

		const { id, ...inputs } = newTask.dataValues

		const response = await request(app)
			.post(`/tasks`)
			.send(inputs)

		expect(response.status).toBe(201)
		const data = response.body.data

		expect(data).toEqual(expect.objectContaining({
			id: expect.any(Number),
			title: expect.any(String),
			body: expect.any(String),
			tags: expect.any(Array),
			user: expect.objectContaining({
				id: expect.any(Number),
				name: expect.any(String),
				email: expect.any(String),
			})
		}))
	})

	it('deve retornar 201 ao cadastrar task associada a tag com dados validos', async () => {

		const newTask = await factory.build('Task')
		const tags = await factory.createMany('Tag', 2)

		const { id, ...inputs } = newTask.dataValues
		inputs.tags = [tags[0].id, tags[1].id]

		const response = await request(app)
			.post(`/tasks`)
			.send(inputs)

		const data = response.body.data
		expect(response.status).toBe(201)

		expect(data).toEqual(expect.objectContaining({
			id: expect.any(Number),
			title: expect.any(String),
			body: expect.any(String),
			tags: expect.arrayContaining([
				expect.objectContaining({
					id: expect.any(Number),
					name: expect.any(String)
				})
			]),
			user: expect.objectContaining({
				id: expect.any(Number),
				name: expect.any(String),
				email: expect.any(String),
			})
		}))
	})

	it('deve ignorar ids de tags nao cadastradas ao cadastrar task', async () => {

		const newTask = await factory.build('Task')
		const tag = await factory.create('Tag')

		const { id, ...inputs } = newTask.dataValues
		inputs.tags = [tag.id, 999]

		const response = await request(app)
			.post(`/tasks`)
			.send(inputs)

		const data = response.body.data
		expect(response.status).toBe(201)

		expect(data).toEqual(expect.objectContaining({
			id: expect.any(Number),
			title: expect.any(String),
			body: expect.any(String),
			tags: expect.arrayContaining([{
				id: tag.id,
				name: tag.name
			}]),
			user: expect.objectContaining({
				id: expect.any(Number),
				name: expect.any(String),
				email: expect.any(String),
			})
		}))
	})

	it.each(dataProvider)
		('deve retornar 422 ao cadastrar uma task %p', async (desc, inputs) => {

			const response = await request(app)
				.post('/tasks')
				.send(inputs)

			expect(response.status).toBe(422)
		})

	it('deve - ao atualizar - alterar os dados da task e manter as tags quando estas nao forem informadas', async () => {

		const task = await factory.create('Task')
		const tags = await factory.createMany('Tag', 2)
		await task.setTags(tags)

		const newData = await factory.build('Task')
		const { id, ...inputs } = newData.dataValues

		const response = await request(app)
			.put(`/tasks/${task.id}`)
			.send(inputs)

		const data = response.body.data

		expect(response.status).toBe(200)

		expect(data).toEqual(expect.objectContaining({
			id: expect.any(Number),
			title: newData.title,
			body: newData.body,
			tags: expect.arrayContaining([
				{
					id: tags[0].id,
					name: tags[0].name
				},
				{
					id: tags[1].id,
					name: tags[1].name
				},
			]),
			user: expect.objectContaining({
				id: newData.user_id,
				name: expect.any(String),
				email: expect.any(String),
			})
		}))
	})

	it('deve - ao atualizar - alterar os dados da task e apagar as associacoes com tags quando passado um array vazio', async () => {

		const task = await factory.create('Task')
		const tags = await factory.createMany('Tag', 2)
		await task.setTags(tags)

		const newData = await factory.build('Task')
		const { id, ...inputs } = newData.dataValues
		inputs.tags = []

		const response = await request(app)
			.put(`/tasks/${task.id}`)
			.send(inputs)

		const data = response.body.data
		expect(response.status).toBe(200)

		expect(data).toEqual(expect.objectContaining({
			id: expect.any(Number),
			title: newData.title,
			body: newData.body,
			tags: expect.arrayContaining([]),
			user: expect.objectContaining({
				id: newData.user_id,
				name: expect.any(String),
				email: expect.any(String),
			})
		}))
	})

	it('deve ignorar ids de tags nao cadastradas ao atualizar task', async () => {

		const task = await factory.create('Task')
		const tag = await factory.create('Tag')

		const { id, ...inputs } = task.dataValues
		inputs.tags = [tag.id, 999]

		const response = await request(app)
			.put(`/tasks/${task.id}`)
			.send(inputs)

		const data = response.body.data
		expect(response.status).toBe(200)

		expect(data).toEqual(expect.objectContaining({
			id: expect.any(Number),
			title: expect.any(String),
			body: expect.any(String),
			tags: expect.arrayContaining([{
				id: tag.id,
				name: tag.name
			}]),
			user: expect.objectContaining({
				id: expect.any(Number),
				name: expect.any(String),
				email: expect.any(String),
			})
		}))
	})

	it('deve retornar 404 ao atualizar task nao cadastrada', async () => {

		const task = await factory.build('Task')
		const { id, ...inputs } = task.dataValues

		const response = await request(app)
			.put(`/tasks/0`)
			.send(inputs)

		expect(response.status).toBe(404)

	})

	it.each(dataProvider)
		('deve retornar 422 ao atualizar uma task %p', async (desc, inputs) => {

			const task = await factory.create('Task')

			const response = await request(app)
				.put(`/tasks/${task.id}`)
				.send(inputs)

			expect(response.status).toBe(422)
		})

	it('deve retornar 200 ao excluir task', async () => {
		const tags = await factory.createMany('Tag', 2)
		const task1 = await factory.create('Task')
		const task2 = await factory.create('Task')
		await task1.setTags(tags)
		await task2.setTags([tags[0].id])


		const response = await request(app)
			.delete(`/tasks/${task2.id}`)
			.send()

		expect(response.status).toBe(200)

		const deletedTask = await Task.findByPk(task2.id)
		expect(deletedTask).toBe(null)
	})

	it('deve retornar 404 ao excluir uma task inexistente', async () => {
		const tag = await factory.create('Tag')

		const response = await request(app)
			.delete('/tasks/0')
			.send()

		expect(response.status).toBe(404)
	})

})