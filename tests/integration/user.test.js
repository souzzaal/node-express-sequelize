const app = require('../../app')
const factory = require('../../database/factories/')
const request = require('supertest')

let user

beforeAll(async () => {
	user = await factory.create('User', {
		email: 'jacadastrado@mail.com'
	});

	return user

});

const dataProvider = [
	// desc, dataset
	['sem dados', {}],
	['com nome vazio', { name: '', email: 'user@mail.com' }],
	['com nome nulo', { name: null, email: 'user@mail.com' }],
	['com email nulo', { name: 'User', email: null }],
	['com email invalido', { name: 'User', email: 'sadf' }],
	['com email já cadastrado', { name: 'User', email: 'jacadastrado@mail.com' }],
]

jest.setTimeout(15000)

describe('User', () => {

	it('deve retornar um array de usuarios quando existem usuarios cadastrados', async () => {

		let users = await factory.createMany('User', 5)

		const response = await request(app)
			.get('/users')
			.send()

		expect(response.status).toBe(200)
		const data = response.body.data

		expect(data.length).toBeGreaterThanOrEqual(1)

		data.forEach(user => {
			expect(user).toEqual(expect.objectContaining({
				id: expect.any(Number),
				name: expect.any(String),
				email: expect.any(String),
				tasks: expect.any(Array)
			}))
		});
	})

	it('deve retornar o usuario com o id especificado na url', async () => {
		let user = await factory.create('User')

		const response = await request(app)
			.get(`/users/${user.id}`)
			.send()

		expect(response.status).toBe(200)
		const data = response.body.data

		expect(user).toEqual(expect.objectContaining({
			id: user.id,
			name: user.name,
			email: user.email
		}))
	})

	it('deve retornar 404 ao buscar usuario inexistente', async () => {

		const response = await request(app)
			.get('/users/0')
			.send()

		expect(response.status).toBe(404)
	})

	it('deve retornar 201 ao cadastrar usuario com sucesso', async () => {

		let user = {
			name: 'André Luiz',
			email: 'andre@mail.com'
		}

		const response = await request(app)
			.post('/users')
			.send(user)
		const data = response.body.data
		expect(response.status).toBe(201)

		expect(data).toEqual(expect.objectContaining({
			id: expect.any(Number),
			name: user.name,
			email: user.email
		}))
	})

	it.each(dataProvider)
		('deve retornar 422 ao cadastrar um usuario %p', async (desc, inputs) => {
			const response = await request(app)
				.post('/users')
				.send(inputs)

			expect(response.status).toBe(422)
		})

	it('deve retornar um usuario atualizar com sucesso', async () => {

		const user = await factory.create('User')

		const newUserData = {
			name: 'André Luiz',
			email: 'andreluiz@mail.com'
		}

		const response = await request(app)
			.put(`/users/${user.id}`)
			.send(newUserData)
		const data = response.body.data

		expect(response.status).toBe(200)

		expect(data).toEqual(expect.objectContaining({
			id: user.id,
			name: newUserData.name,
			email: newUserData.email
		}))
	})

	it.each(dataProvider)
		('deve retornar 422 ao atualizar um usuario %p', async (desc, inputs) => {

			const user = await factory.create('User')

			const response = await request(app)
				.put(`/users/${user.id}`)
				.send(inputs)

			expect(response.status).toBe(422)
		})

	it('deve retornar 404 ao atualizar usuario inexistente', async () => {

		const newUserData = {
			name: 'André Luiz',
			email: 'luiz@mail.com'
		}

		const response = await request(app)
			.put(`/users/0`)
			.send(newUserData)

		expect(response.status).toBe(404)
	})

	it('deve excluir um usuario quando nao associado com tafefa', async () => {

		const user = await factory.create('User')

		const response = await request(app)
			.delete(`/users/${user.id}`)
			.send()

		expect(response.status).toBe(200)
	})

	it('deve retornar 422 ao excluir usuario associado com tafefa', async () => {

		const task = await factory.create('Task')

		const response = await request(app)
			.delete(`/users/${task.user_id}`)
			.send()

		expect(response.status).toBe(422)
	})

	it('deve retornar 404 ao excluir usuario inexistente', async () => {
		const response = await request(app)
			.delete(`/users/0`)
			.send()

		expect(response.status).toBe(404)
	})

})