const app = require('../../app')
const factory = require('../../database/factories/')
const request = require('supertest')
const { Tag, Task } = require('../../app/models')

jest.setTimeout(15000)

describe('Tags', () => {

	it('deve retornar array de tags quando existem tags cadastradas', async () => {

		await factory.createMany('Tag', 5)

		const response = await request(app)
			.get('/tags')
			.send()

		expect(response.status).toBe(200)
		const data = response.body.data
		expect(data.length).toBeGreaterThanOrEqual(1)

		// expect(data).toEqual(expect.arrayContaining([expect.objectContaining({
		// 	name: expect.any(String)
		// })]))

		data.forEach(tag => {
			expect(tag).toEqual(expect.objectContaining({
				name: expect.any(String)
			}))
		});
	})

	it('deve retornar tag existente', async () => {
		const tag = await factory.create('Tag')

		const response = await request(app)
			.get(`/tags/${tag.id}`)
			.send()

		const data = response.body.data

		expect(response.status).toBe(200)
		expect(data).toEqual(expect.objectContaining({
			id: tag.id,
			name: tag.name
		}))
	})

	it('deve retornar 404 ao buscar tag inexistente', async () => {
		const response = await request(app)
			.get('/tags/100')
			.send()

		expect(response.status).toBe(404)
	})

	it('deve retornar uma tag ao cadastrar com dados validos', async () => {

		const tag = await factory.build('Tag')

		const response = await request(app)
			.post('/tags')
			.send({ name: tag.name })

		const data = response.body.data

		expect(response.status).toBe(201)
		expect(data).toEqual(expect.objectContaining({
			id: expect.any(Number),
			name: tag.name
		}))
	})

	it.each([
		// desc, dataset
		['sem dados', {}],
		['com nome vazio', { name: '' }],
		['com nome nulo', { name: null }]
	])('deve retornar 422 ao cadastrar uma tag %p', async (desc, inputs) => {

		const response = await request(app)
			.post('/tags')
			.send(inputs)

		expect(response.status).toBe(422)
	})

	it('deve retornar uma tag ao atualizar com dados validos', async () => {

		const tag = await factory.create('Tag')

		const response = await request(app)
			.put(`/tags/${tag.id}`)
			.send({ name: 'asdf' })

		const data = response.body.data

		expect(response.status).toBe(200)
		expect(data).toEqual(expect.objectContaining({
			id: tag.id,
			name: 'asdf'
		}))
	})

	it('deve retornar 404 ao atualizar tag inexistente', async () => {

		const response = await request(app)
			.put('/tags/0')
			.send({ name: 'asdf' })

		const data = response.body.data

		expect(response.status).toBe(404)
	})

	it.each([
		// desc, dataset
		['sem dados', {}],
		['com nome vazio', { name: '' }],
		['com nome nulo', { name: null }]
	])('deve retornar 422 ao atualizar uma tag %p', async (desc, inputs) => {

		const tag = await factory.create('Tag')

		const response = await request(app)
			.put(`/tags/${tag.id}`)
			.send(inputs)

		expect(response.status).toBe(422)
	})

	it('deve excluir uma tag quando nao associada com task', async () => {
		const tag = await factory.create('Tag')

		const response = await request(app)
			.delete(`/tags/${tag.id}`)
			.send()

		expect(response.status).toBe(200)
		const tagExcluida = await Tag.findByPk(tag.id)
		expect(tagExcluida).toBe(null)
	})

	it('deve retornar 422 ao excluir tag associada com task', async () => {

		const tag = await factory.create('Tag')
		const task = await factory.create('Task')
		await task.setTags([tag])

		const response = await request(app)
			.delete(`/tags/${tag.id}`)
			.send()

		expect(response.status).toBe(422)

	})

	it('deve retornar 404 ao excluir uma tag inexistente', async () => {
		const tag = await factory.create('Tag')

		const response = await request(app)
			.delete('/tags/0')
			.send()

		expect(response.status).toBe(404)
	})

})