const { param, body, validationResult } = require('express-validator')
const { Task, User } = require('../models')
const { Op } = require("sequelize");

class UserRequest {

	rules = (method) => {

		let rules = [];

		switch (method) {
			case 'POST':
			case 'PUT':
				rules = [
					body('name').trim().escape().notEmpty(),
					body('email').trim().escape().isEmail().normalizeEmail()
						.custom(async (email, { req }) => {

							const conditions = {
								email: {
									[Op.eq]: email
								}
							}

							if (method == 'PUT') {
								conditions.id = {
									[Op.ne]: param('id')
								}
							}

							const user = await User.findAll({
								where: conditions
							})

							if (user.length > 0) {
								throw new Error('Email já cadastrado')
							}

							return true
						})
				]
				break;
			case 'DELETE':
				rules = [
					param('id').custom(async (id, { req }) => {

						const tasks = await Task.findAll({
							where: {
								user_id: id
							}
						})

						if (tasks.length > 0) {
							throw new Error('O usuário possui tarefas')
						}

						return true
					})
				]
				break
		}

		return rules
	}

}

module.exports = new UserRequest