const { param, body, validationResult } = require('express-validator')
const { Tag, Task } = require('../models')

class TagRequest {

	rules = (method) => {

		let rules = null;

		switch (method) {
			case 'POST':
			case 'PUT':
				rules = [
					body('name').trim().escape().notEmpty()
				]
				break;
			case 'DELETE':
				rules = [
					param('id').custom(async (id, { req }) => {

						const task = await Task.findAll({
							include: [
								{
									model: Tag,
									as: 'tags',
									where: { id: id },
									through: { attributes: [] },			// não retorna os campos da tabela pivo
									attributes: ['id', 'name'],				// seleciona campos da tabela associada
									required: true,							// retorna apenas Tasks que tenham associacao com Tags (inner join)
								},
							]
						})

						if (task.length > 0) {
							throw new Error('A Tag está associada com uma ou mais Tasks')
						}

						return true
					})
				]
				break;
		}


		return rules
	}
}

module.exports = new TagRequest()