const { param, body, validationResult } = require('express-validator')
const { Tag, Task, User } = require('../models')

class TaskRequest {

	rules = (method) => {

		let rules = null;

		switch (method) {
			case 'POST':
			case 'PUT':
				rules = [
					body('title').trim().escape().notEmpty(),
					body('body').trim().escape().notEmpty(),
					body('user_id').trim().escape().notEmpty().isNumeric().custom(async (user_id, { req }) => {

						const user = await User.findByPk(user_id)

						if (!user) {
							throw new Error('Usuario não cadastrado')
						}

						return true
					}),
					body('tags').optional().isArray(),
				]
				break
		}

		return rules
	}
}

module.exports = new TaskRequest()