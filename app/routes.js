const { Router } = require('express')
const Request = require('./requests/Request')
const TagController = require('./controllers/TagController')
const TaskController = require('./controllers/TaskController')
const TagRequest = require('./requests/TagRequest')
const TaskRequest = require('./requests/TaskRequest')
const UserController = require('./controllers/UserController')
const UserRequest = require('./requests/UserRequest')

const GET = 'GET'
const POST = 'POST'
const PUT = 'PUT'
const DELETE = 'DELETE'

const routes = Router()

// Tags
routes.get('/tags', TagController.index)
routes.get('/tags/:id', TagController.show)
routes.post('/tags', TagRequest.rules(POST), Request.validate, TagController.store)
routes.put('/tags/:id', TagRequest.rules(PUT), Request.validate, TagController.update)
routes.delete('/tags/:id', TagRequest.rules(DELETE), Request.validate, TagController.destroy)

// Tasks
routes.get('/tasks', TaskController.index)
routes.get('/tasks/:id', TaskController.show)
routes.post('/tasks', TaskRequest.rules(POST), Request.validate, TaskController.store)
routes.put('/tasks/:id', TaskRequest.rules(POST), Request.validate, TaskController.update)
routes.delete('/tasks/:id', TaskController.destroy)

// Users
routes.get('/users', UserController.index);
routes.get('/users/:id', UserController.show);
routes.post('/users', UserRequest.rules(POST), Request.validate, UserController.store);
routes.put('/users/:id', UserRequest.rules(PUT), Request.validate, UserController.update);
routes.delete('/users/:id', UserRequest.rules(DELETE), Request.validate, UserController.destroy);


module.exports = routes;