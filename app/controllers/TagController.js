const { Tag } = require('../models')

class TagController {
	async index(req, res) {
		try {
			const tags = await Tag.findAll()

			return res.json({ data: tags })
		} catch (error) {
			return res.status(500).json({ errors: error.message })
		}
	}

	async show(req, res) {
		try {
			const tag = await Tag.findByPk(req.params.id)

			if (!tag) {
				return res.status(404).json({ errors: 'Tag não encontrada' })
			}

			return res.json({ data: tag })
		} catch (error) {
			return res.status(500).json({ errors: error.message })
		}
	}

	async store(req, res) {
		try {
			const tag = await Tag.create(req.body)

			return res.status(201).json({ data: tag })
		} catch (error) {

			return res.status(500).json({ errors: error.message })
		}
	}

	async update(req, res) {
		try {
			const tag = await Tag.findByPk(req.params.id)

			if (!tag) {
				return res.status(404).json({ errors: 'Tag não encontrada' })
			}

			await tag.update(req.body)

			return res.json({ data: tag })
		} catch (error) {
			res.status(500).json({ errors: error.message })
		}
	}

	async destroy(req, res) {
		try {
			const tag = await Tag.findByPk(req.params.id)

			if (!tag) {
				return res.status(404).json({ errors: 'Tag não encontrada' })
			}

			await tag.destroy()

			return res.json()
		} catch (error) {
			return res.status(500).json({ errors: error.message })
		}
	}
}

module.exports = new TagController()