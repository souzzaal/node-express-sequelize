const { User } = require('../models')

class UserController {
	async index(req, res) {
		try {
			const users = await User.findAll(
				{ include: ['tasks'] }		// retorna o relacionmanto com tasks
			)

			return res.json({ data: users })
		} catch (error) {
			return res.status(500).json({ errors: error.message })
		}
	}

	async show(req, res) {
		try {
			const user = await User.findByPk(req.params.id);

			if (!user) {
				return res.status(404).json({ errors: 'Usuário não encontrado' })
			}

			return res.json({ data: user });
		} catch (error) {
			return res.status(500).json({ errors: error.message });
		}
	}

	async store(req, res) {
		try {
			const user = await User.create(req.body);

			return res.status(201).json({ data: user });
		} catch (error) {
			return res.status(500).json({ errors: error.message });
		}
	}

	async update(req, res) {
		try {
			const user = await User.findByPk(req.params.id);

			if (!user) {
				return res.status(404).json({ errors: 'Usuário não encontrado' })
			}

			await user.update(req.body);

			return res.json({ data: user });
		} catch (error) {
			return res.status(500).json({ errors: error.message });
		}
	}

	async destroy(req, res) {
		try {
			const user = await User.findByPk(req.params.id);

			if (!user) {
				return res.status(404).json({ errors: 'Usuário não encontrado' })
			}

			await user.destroy();

			return res.json();
		} catch (error) {
			return res.status(500).json({ errors: error.message });
		}
	}
}

module.exports = new UserController()