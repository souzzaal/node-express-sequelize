const { Task, Tag, User } = require('../models')
const db = require('../models')
const { Op } = require("sequelize");

class TaskController {

	async index(req, res) {
		try {
			const whereTask = {}
			const whereTag = {}

			if (req.query.filter) {

				const filter = req.query.filter

				if (filter.user && parseInt(filter.user) > 0) {
					whereTask.user_id = parseInt(filter.user)
				}

				if (filter.tag && filter.tag.length > 0) {

					whereTag.name = {
						[Op.like]: filter.tag
					}
				}
			}

			const task = await Task.findAll({
				where: whereTask,
				include: [
					{
						model: Tag,
						as: 'tags',
						where: whereTag,
						through: { attributes: [] },			// não retorna os campos da tabela pivo
						attributes: ['id', 'name'],				// seleciona campos da tabela associada
					}, {
						model: User,
						as: 'user',
						attributes: ['id', 'name', 'email']		// seleciona campos da tabela associada
					}
				]
			})

			return res.json({ data: task })
		} catch (error) {
			return res.status(500).json({ errors: error.message })
		}
	}

	async show(req, res) {
		try {
			const task = await Task.findByPk(req.params.id, {
				include: [
					{
						model: Tag,
						as: 'tags',
						through: { attributes: [] },			// não retorna os campos da tabela pivo
						attributes: ['id', 'name']				// seleciona campos da tabela associada
					}, {
						model: User,
						as: 'user',
						attributes: ['id', 'name', 'email']		// seleciona campos da tabela associada
					}
				]
			})

			if (task) {
				return res.json({ data: task })
			} else {
				return res.status(404).json({ errors: 'Task not found' })
			}

		} catch (error) {
			return res.status(500).json({ errors: error.message })
		}
	}

	store = async (req, res) => {								// usando arrow functions para utilizar o 'this'

		const transaction = await db.sequelize.transaction()

		try {
			const { tags, ...data } = req.body
			let task = await Task.create(data, { transaction })

			if (tags) {
				let tagsInstances = await Tag.findAll({
					where: {
						id: tags								// açucar sintatico para [Op.in]
					}
				})
				await task.setTags(tagsInstances, { transaction })
			}
			await transaction.commit()

			// Até o momento não encontrei um modo de fazer o lazy load na propria instancia
			task = await task.reload({
				include: [
					{
						model: Tag,
						as: 'tags',
						through: { attributes: [] },			// não retorna os campos da tabela pivo
						attributes: ['id', 'name']				// seleciona campos da tabela associada
					}, {
						model: User,
						as: 'user',
						attributes: ['id', 'name', 'email']		// seleciona campos da tabela associada
					}
				]
			})

			return res.status(201).json({ data: task })

		} catch (error) {
			await transaction.rollback()
			return res.status(500).json({ errors: error.message })
		}
	}

	update = async (req, res) => {
		try {

			const transaction = await db.sequelize.transaction()

			const { tags, ...data } = req.body

			let task = await Task.findByPk(req.params.id)

			if (task) {

				await task.update(data, { transaction })

				if (tags) {
					let tagsInstances = await Tag.findAll({
						where: {
							id: tags
						}
					})
					await task.setTags(tagsInstances, { transaction })		// recria as associacoes entre tasks e tags
				}
				await transaction.commit()

				task = await task.reload({
					include: [
						{
							model: Tag,
							as: 'tags',
							through: { attributes: [] },			// não retorna os campos da tabela pivo
							attributes: ['id', 'name']				// seleciona campos da tabela associada
						}, {
							model: User,
							as: 'user',
							attributes: ['id', 'name', 'email']		// seleciona campos da tabela associada
						}
					]
				})

				return res.status(200).json({ data: task })

			} else {
				return res.status(404).json({ errors: 'Task not found' })
			}


		} catch (error) {
			await transaction.rollback()
			res.status(500).json({ errors: error.message })
		}
	}

	async destroy(req, res) {
		try {
			const transaction = await db.sequelize.transaction()

			const task = await Task.findByPk(req.params.id)
			if (task) {
				await task.setTags([], { transaction })							//  Exclui todas as associções na tabela pivô

				await task.destroy({ transaction })
			} else {
				return res.status(404).json({ errors: 'Task not found' })
			}
			await transaction.commit()

			return res.json({})
		} catch (error) {
			await transaction.rollback()
			return res.status(500).json({ errors: error })
		}

	}

}

module.exports = new TaskController();