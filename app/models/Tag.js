'use strict';
module.exports = (sequelize, DataTypes) => {
	const tag = sequelize.define('Tag', {
		name: {
			type: DataTypes.STRING,
			allowNull: false,					// torna o campo obrigatório
			validate: {
				notNull: {
					msg: 'Required field'		// mensagem de erro de validacao do allowNull
				},
				notEmpty: {
					args: true,					// verifica que o campo não é uma string vazia
					msg: 'Required field'		// mensagem de erro de validacao
				}
			}
		}
	}, {
		underscored: true,						// força o uso do snake_case
		tableName: 'tags',						// nome da tabela no banco
		freezeTableName: true					// desabilita a pluralização do nome da tabela. Por padrão, o sequelize usa o plural do nome do modelo.
	});

	tag.associate = function (models) {
		tag.belongsToMany(models.Task, {
			through: 'tasks_tags',          	// nome da tabela pivô
			as: 'tasks',                    	// apelido do relacionamento
			foreignKey: 'tags_id',         		// chave estrageira do modelo relacionado na tabela pivõ
			timestamps: false,					// desconsidera os campos de timestamps no JOIN com a tabela pivô, quando eles não existem
			unique: false,						// desabilita verificacao de duplicidade das foreignKeys na tabela pivô
		})
	};
	return tag;
};