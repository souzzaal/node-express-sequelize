'use strict';
module.exports = (sequelize, DataTypes) => {
	const task = sequelize.define('Task', {
		title: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				notEmpty: {
					msg: 'Required field'
				}
			}
		},
		body: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				notEmpty: {
					msg: 'Required field'
				}
			}
		},
		user_id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			validate: {
				notEmpty: {
					msg: 'Required field'
				}
			}
		}
	}, {
		underscored: true,					// força o uso do snake_case
		tableName: 'tasks',					// nome da tabela no banco
		freezeTableName: true				// desabilita a pluralização do nome da tabela. Por padrão, o sequelize usa o plural do nome do modelo.
	});

	task.associate = function (models) {
		task.belongsToMany(models.Tag, {
			through: 'tasks_tags',          // nome da tabela pivô
			as: 'tags',                     // apelido do relacionamento
			foreignKey: 'tasks_id',         // chave estrageira do modelo relacionado na tabela pivõ
			// outherKey: 'tags_id',		// chave estrageira do outro modelo relacionado na tabela pivô
			timestamps: false,				// desconsidera os campos de timestamps no JOIN com a tabela pivô, quando eles não existem,
			unique: false,					// desabilita verificacao de duplicidade das foreignKeys na tabela pivô
		})

		task.belongsTo(models.User, {
			as: 'user',
			foreignKey: 'user_id'
		})
	};
	return task;
};