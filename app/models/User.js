'use strict';
module.exports = (sequelize, DataTypes) => {
	const user = sequelize.define('User', {
		name: {
			type: DataTypes.STRING,
			allowNull: false,					// torna o campo obrigatório
			validate: {
				notNull: {
					msg: 'Required field' 		// mensagem de erro de validacao do allowNull
				},
				notEmpty: {
					args: true,					// verifica que o campo não é uma string vazia
					msg: 'Required field'		// mensagem de erro de validacao
				}
			}
		},
		email: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				notNull: {
					msg: 'Required field'
				},
				isEmail: {
					args: true,
					msg: 'Invalid e-mail'
				}
			}
		}
	}, {
		underscored: true,				// força o uso do snake_case
		tableName: 'users',				// nome da tabela no banco
		freezeTableName: true			// desabilita a pluralização do nome da tabela. Por padrão, o sequelize usa o plural do nome do modelo.
	});

	user.associate = function (models) {
		user.hasMany(models.Task, {
			as: 'tasks',				// nome do relacionamento
			foreignKey: 'user_id'		// chave estrageira do modelo relacionado na tabela relacionada
		})
	};
	return user;
};