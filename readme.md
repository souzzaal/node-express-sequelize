# API com NodeJS: Express, Sequelize e Jest

## Configuração do Sequelize  

Crie o projeto

> `yarn init`

Adicione as dependências necessárias

> `yarn add express sequelize sqlite3 dotenv-safe`

> `yarn add sequelize-cli -D`

Na raiz do projeto criar o arquivo `.sequelizerc`

Preencha o arquivo com o seguinte conteúdo, alterando conforme necessidade

> ``` javascript
> const path = require('path');
>
> module.exports = {
>    'config': path.resolve('config', 'database.js'),
>    'models-path': path.resolve('app', 'models'),
>    'seeders-path': path.resolve('database', 'seeders'),
>    'migrations-path': path.resolve('database', 'migrations')
>}; 
> ```

Para gerar a estrutura de pastas do arquivo .sequelizerc execute

> `npx sequelize init`

Converta `config/database.js` em um módulo alterando o conteúdo para

>    ``` javascript
>    module.exports = {
>    "development": {
>        "username": "root",
>        "password": null,
>        "database": "database_development",
>        "host": "127.0.0.1",
>        "dialect": "mysql",
>        "operatorsAliases": false
>    },
>    "test": {
>        "username": "root",
>        "password": null,
>        "database": "database_test",
>        "host": "127.0.0.1",
>        "dialect": "mysql",
>        "operatorsAliases": false
>    },
>    "production": {
>        "username": "root",
>        "password": null,
>       "database": "database_production",
>        "host": "127.0.0.1",
>        "dialect": "mysql",
>        "operatorsAliases": false
>    }
>    }
>    ```

Para tornar dinâmicas as configurações de acesso ao banco de dados altere o conteudo de `config/database.js` para

>    ``` javascript
>    require('dotenv-safe').config({
>        path: process.env.NODE_ENV == 'test' ? '.env.test' : '.env'
>    })
>
>    module.exports = {
>        "host": process.env.DB_HOST,
>        "database": process.env.DB_NAME,
>        "username": process.env.DB_USER,
>        "password": process.env.DB_PASS,
>        "dialect": process.env.DB_DIALECT,
>        "storage": process.env.DB_STORAGE,
>        "logging": process.env.DB_LOGGING || false,    // false: deixa de exibir as queries SQL no terminal
>        define: {
>            timestamps: true,                          // força a criação dos campos created_at e updated_at
>            underscored: true,                         // força que os campos acima sejam criados em snake_case (camelCase é o padrão)
>            underscoredAll: true,                      // força todos os campos para snake_case
>        }
>    }
>    ```

Na raiz do projeto crie os arquivos `.env.example`, `.env` e `.env.test`. O arquivo .env.example deve conter todas variáveis de ambiente que o sistema necessita. Os outros arquivos .env devem conter as mesmas variáveis com os respectivos valores de cada ambiente. Caso uma variável não exista no .env o NodeJS disparará uma exceção.

Feito isso é preciso alterar o carregamento da configuração do BD no arquivo `models/index.js` alterando 

> `const config = require(__dirname + '/../../config/database.js')[env]`

para 

> `const config = require('../../config/database')`

### Criando e executando migrations

Para criar uma migration execute

> `npx sequelize migration::create --name=Nome`

É possível gerar uma migration ao mesmo tempo em que se cria um model, para tanto execute

> `npx sequelize model:create --name=ModelName --attributes=att1:integer,att2:string --undescored`

>> --atributes: defines os campos do modelo, att1 e att2 são os nomes. Não pode haver espaços

>> --undescored: força o uso do snake_case para os campos created_at e updated_at, por padrão é utilizado camelCase 

Para executar uma migration execute

> `npx sequelize db:migrate`

>> utilize o parametro `--env=nomeEnv` para definir o ambiente em que será executada a migração

## Configuração do Jest

> `yarn add jest -D`

Após a instalação execute o seguinte comando para gerar o arquivo de configuração (jest.config.js) do Jest na raiz do projeto.

> `jest --init`

Por padrão o Jest vai buscar os testes na pasta "\_\_testes\_\_", caso deseje mudar procure por "testMatch" no arquivo de configuração. Abaixo um exemplo de como buscar arquivos na pasta "teste" executando apenas arquivos com a extensão ".test.js":

> `"**/tests/**/*.test.js?(x)"`

Para facilitar a execução dos testes criar um comando personalizado. Na seção scripts do arquivo package.json adicione:

> `"test": "NODE_ENV=test jest"`

Caso deseje limpar a base de dados antes de iniciar os testes, adicone na na seção scripts do package.json

> `"pretest": "NODE_ENV=test sequelize db:migrate:undo:all && NODE_ENV=test sequelize db:migrate",`

Para executa todos os testes

> `yarn test`

### Configurando projeto para executar os testes

É necessário separar a aplicação do serviço que disponibiliza a aplicação. Isso porque para executar os testes não é preciso levantar a aplicação.

Para isso no arquivo `app/index.js` deve estar o código que carrega a aplicação:

>```javascript
>    require('dotenv-safe').config({
>        path: process.env.NODE_ENV == 'test' ? '.env.test' : '.env'
>    })
>    const express = require('express')
>    const routes = require('./routes')
>    const app = new express()
>    app.use(express.json())
>    app.use(routes)
>    module.exports = app
>```

Ja o arquivo `index.js` na raiz do projeto deve carregar e disponibilizar a aplicação:

>```javascript
>    const app = require('./app')
>    app.listen(process.env.PORT || 3000)
>```

### Configurando Visual Studio Code para executar um teste específico

Na raiz do projeto crie uma pasta chamada `.vscode` e dentro dela um arquivo chamado `launch.json`, que deve ter o seguinte conteúdo

> ```javascript
>
>    {
>    "configurations": [
>        {
>           "type": "node",
>           "request": "launch",
>           "name": "Run Jest file test",
>           "runtimeExecutable": "npm",
>           "runtimeArgs": [
>               "run-script",
>               "test"
>           ],
>           "args": [
>               "--",
>               "-i",
>               "${relativeFile}"
>          ],
>          "console": "integratedTerminal"
>        },
>        {
>            "type": "node",
>            "request": "launch",
>            "name": "Run selected Jest test",
>            "runtimeExecutable": "npm",
>            "runtimeArgs": [
>                "run-script",
>                "test"
>            ],
>            "args": [
>                "--",
>                "-i",
>                "${relativeFile}",
>                "-t",
>                "${selectedText}"
>            ],
>            "console": "integratedTerminal",
>        }
>    ]
>    }
>```

Caso o arquivo já exista, adicione o objeto apresentado no array `configurations`.

Para executar um teste específico, selecione sua descrição ou parte dela (primeiro argumento da função `it`) e, em seguida, aperte `F5`. Para executar todos os testes do arquivo aperte `F5`