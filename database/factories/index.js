const faker = require('faker/locale/pt_BR')
const { factory } = require('factory-girl')
const { Tag, Task, User } = require('../../app/models')

factory.define('Tag', Tag, {
	name: faker.lorem.word
})

factory.define('Task', Task, {
	title: faker.lorem.words,
	body: faker.lorem.paragraph,
	user_id: factory.assoc('User', 'id')
})

factory.define('User', User, {
	name: faker.name.findName,
	email: faker.internet.email
})

module.exports = factory