'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {

		return queryInterface.createTable('tasks_tags', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER,
			},
			tasks_id: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'tasks',
					key: 'id'
				},
			},
			tags_id: {
				allowNull: false,
				type: Sequelize.INTEGER,
				references: {
					model: 'tags',
					key: 'id'
				}
			}
		});
	},

	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('tasks_tags');
	}
};
